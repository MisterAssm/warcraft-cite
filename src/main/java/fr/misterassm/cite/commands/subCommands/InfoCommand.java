package fr.misterassm.cite.commands.subCommands;

import fr.misterassm.cite.commands.ISubCommand;
import fr.misterassm.cite.managers.ConfigurationManager;
import fr.misterassm.cite.managers.cite.AbstractCite;
import fr.misterassm.cite.managers.cite.CiteData;
import fr.misterassm.cite.managers.player.PlayerCite;
import fr.misterassm.cite.managers.player.PlayerData;
import org.bukkit.entity.Player;

public class InfoCommand extends ISubCommand {

    @Override
    public String getName() {
        return "info";
    }

    @Override
    public void execute(Player player, String[] args) {

        PlayerData playerData = PlayerCite.getInstance().getCiteByPlayer(player.getName());
        CiteData citeData = AbstractCite.getInstance().getCiteByName(playerData.getCite());

        player.sendMessage(ConfigurationManager.getMessage("lang.citeinfo")
                .replaceAll("\\{cite.kills}", String.valueOf(AbstractCite.getInstance().getKills(citeData)))
                .replaceAll("\\{cite.deaths}", String.valueOf(AbstractCite.getInstance().getDeaths(citeData)))
                .replaceAll("\\{cite.victory}", String.valueOf(citeData.getVictory()))
                .replaceAll("\\{cite.defeat}", String.valueOf(citeData.getDefeat()))

                .replaceAll("\\{player.kills}", String.valueOf(playerData.getKills()))
                .replaceAll("\\{player.deaths}", String.valueOf(playerData.getDeaths())));

    }
}
