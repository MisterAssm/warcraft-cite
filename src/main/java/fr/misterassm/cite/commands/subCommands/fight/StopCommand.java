package fr.misterassm.cite.commands.subCommands.fight;

import fr.misterassm.cite.Cite;
import fr.misterassm.cite.commands.ISubCommand;
import fr.misterassm.cite.enums.FightState;
import fr.misterassm.cite.managers.ConfigurationManager;
import fr.misterassm.cite.managers.fight.Fight;
import fr.misterassm.cite.managers.fight.exceptions.FightNotStartedException;
import org.bukkit.entity.Player;

public class StopCommand extends ISubCommand {

    @Override
    public String getName() {
        return "stop";
    }

    @Override
    public void execute(Player player, String[] args) {
        if (!player.hasPermission("cite.admin")) {
            player.sendMessage(ConfigurationManager.getMessage("lang.noPermission"));
            return;
        }

        if (Cite.getInstance().getFightState().equals(FightState.NOT_START)) {

            player.sendMessage(ConfigurationManager.getMessage("fight.admin.stopNotStart"));

        } else {

            try {
                Fight.getInstance().forceStop();
            } catch (FightNotStartedException e) {
                e.printStackTrace();
            }

        }
    }
}
