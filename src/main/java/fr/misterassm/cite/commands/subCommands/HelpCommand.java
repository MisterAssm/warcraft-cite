package fr.misterassm.cite.commands.subCommands;

import fr.misterassm.cite.commands.ISubCommand;
import fr.misterassm.cite.managers.ConfigurationManager;
import org.bukkit.entity.Player;

public class HelpCommand extends ISubCommand {

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public void execute(Player player, String[] args) {

        player.sendMessage(ConfigurationManager.getMessage("lang.help"));

    }
}
