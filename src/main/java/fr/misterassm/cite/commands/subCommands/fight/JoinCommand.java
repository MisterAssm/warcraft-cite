package fr.misterassm.cite.commands.subCommands.fight;

import fr.misterassm.cite.Cite;
import fr.misterassm.cite.commands.ISubCommand;
import fr.misterassm.cite.enums.FightState;
import fr.misterassm.cite.managers.ConfigurationManager;
import fr.misterassm.cite.managers.fight.Fight;
import fr.misterassm.cite.managers.fight.exceptions.AlreadyInFightException;
import fr.misterassm.cite.managers.fight.exceptions.FightNotStartedException;
import org.bukkit.entity.Player;

public class JoinCommand extends ISubCommand {

    @Override
    public String getName() {
        return "join";
    }

    @Override
    public void execute(Player player, String[] args) {
        if (!Cite.getInstance().getFightState().equals(FightState.WAITING)) {
            player.sendMessage(ConfigurationManager.getMessage("fight.join.notWaitingPhase"));
            return;
        }

        if (Fight.getInstance().hasFight(player)) {
            player.sendMessage(ConfigurationManager.getMessage("fight.join.alreadyInFight"));
            return;
        }

        try {
            Fight.getInstance().addPlayer(player);
        } catch (AlreadyInFightException | FightNotStartedException e) {
            e.printStackTrace();
        }

    }
}
