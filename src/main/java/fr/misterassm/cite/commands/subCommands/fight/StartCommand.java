package fr.misterassm.cite.commands.subCommands.fight;

import fr.misterassm.cite.Cite;
import fr.misterassm.cite.commands.ISubCommand;
import fr.misterassm.cite.enums.FightState;
import fr.misterassm.cite.managers.ConfigurationManager;
import fr.misterassm.cite.managers.fight.Fight;
import fr.misterassm.cite.managers.fight.exceptions.FightAlreadyStartedException;
import org.bukkit.entity.Player;

public class StartCommand extends ISubCommand {

    @Override
    public String getName() {
        return "start";
    }

    @Override
    public void execute(Player player, String[] args) {
        if (!player.hasPermission("cite.admin")) {
            player.sendMessage(ConfigurationManager.getMessage("lang.noPermission"));
            return;
        }

        if (Cite.getInstance().getFightState().equals(FightState.NOT_START)) {

            try {
                Fight.getInstance().startWaiting();
            } catch (FightAlreadyStartedException e) {
                e.printStackTrace();
            }

        } else if (Cite.getInstance().getFightState().equals(FightState.WAITING)) {

            Fight.getInstance().startFight();
            player.sendMessage(ConfigurationManager.getMessage("fight.admin.startingFight"));

        } else if (Cite.getInstance().getFightState().equals(FightState.FIGHT)) {

            player.sendMessage(ConfigurationManager.getMessage("fight.admin.alreadyInFight"));

        }
    }
}
