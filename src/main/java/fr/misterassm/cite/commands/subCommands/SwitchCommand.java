package fr.misterassm.cite.commands.subCommands;

import fr.misterassm.cite.commands.ISubCommand;
import fr.misterassm.cite.inventory.SwitchInventory;
import org.bukkit.entity.Player;

public class SwitchCommand extends ISubCommand {

    @Override
    public String getName() {
        return "switch";
    }

    @Override
    public void execute(Player player, String[] args) {

        new SwitchInventory(player).open(player);

    }
}
