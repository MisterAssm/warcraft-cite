package fr.misterassm.cite.commands.subCommands;

import fr.misterassm.cite.Cite;
import fr.misterassm.cite.commands.ISubCommand;
import fr.misterassm.cite.enums.CiteEnum;
import fr.misterassm.cite.inventory.JoinInventory;
import fr.misterassm.cite.managers.ConfigurationManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class JoinCommand extends ISubCommand {

    @Override
    public String getName() {
        return "join";
    }

    @Override
    public void execute(Player player, String[] args) {

        if (args.length < 2) {
            player.sendMessage(ConfigurationManager.getMessage("commands.join.invalidArgument"));
        } else {

            if (args[1].equalsIgnoreCase("athenes")) {
                Bukkit.getScheduler().runTask(Cite.getInstance(), () -> new JoinInventory(CiteEnum.ATHENES).open(player));
            } else if (args[1].equalsIgnoreCase("alexandrie")) {
                Bukkit.getScheduler().runTask(Cite.getInstance(), () -> new JoinInventory(CiteEnum.ALEXANDRIE).open(player));
            }

        }
    }
}
