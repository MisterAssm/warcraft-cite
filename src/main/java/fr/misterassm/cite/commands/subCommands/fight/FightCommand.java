package fr.misterassm.cite.commands.subCommands.fight;

import fr.misterassm.cite.commands.ISubCommand;
import fr.misterassm.cite.managers.ConfigurationManager;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class FightCommand extends ISubCommand {

    private List<ISubCommand> iSubCommands = new ArrayList<>();

    public FightCommand() {
        iSubCommands.add(new JoinCommand());
        iSubCommands.add(new StartCommand());
        iSubCommands.add(new StopCommand());
    }

    @Override
    public String getName() {
        return "fight";
    }

    @Override
    public void execute(Player player, String[] args) {

        System.out.println(args.length);

        if (args.length > 1) {
            for (int i = 0; i < this.getiSubCommands().size(); i++) {
                if (args[1].equalsIgnoreCase(this.getiSubCommands().get(i).getName())) {
                    this.getiSubCommands().get(i).execute(player, args);
                }
            }
        } else {
            player.sendMessage(ConfigurationManager.getMessage("fight.help"));
        }
    }

    public List<ISubCommand> getiSubCommands() { return iSubCommands; }
}
