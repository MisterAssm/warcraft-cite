package fr.misterassm.cite.commands;

import fr.misterassm.cite.commands.subCommands.HelpCommand;
import fr.misterassm.cite.commands.subCommands.InfoCommand;
import fr.misterassm.cite.commands.subCommands.JoinCommand;
import fr.misterassm.cite.commands.subCommands.SwitchCommand;
import fr.misterassm.cite.commands.subCommands.fight.FightCommand;
import fr.misterassm.cite.managers.ConfigurationManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class CiteCommand implements CommandExecutor {

    private List<ISubCommand> iSubCommands = new ArrayList<>();

    public CiteCommand() {
        iSubCommands.add(new HelpCommand());
        iSubCommands.add(new InfoCommand());
        iSubCommands.add(new SwitchCommand());
        iSubCommands.add(new JoinCommand());

        iSubCommands.add(new FightCommand());
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (commandSender instanceof Player) {
            Player player = (Player) commandSender;

            if (args.length > 0) {
                for (int i = 0; i < this.getiSubCommands().size(); i++) {
                    if (args[0].equalsIgnoreCase(this.getiSubCommands().get(i).getName())) {
                        this.getiSubCommands().get(i).execute(player, args);
                    }
                }
            } else {
                player.sendMessage(ConfigurationManager.getMessage("lang.help"));
            }
        }

        return true;
    }

    public List<ISubCommand> getiSubCommands() { return iSubCommands; }

}
