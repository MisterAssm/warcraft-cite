package fr.misterassm.cite.commands;

import org.bukkit.entity.Player;

public abstract class ISubCommand {

    public abstract String getName();

    public abstract void execute(Player player, String args[]);

}
