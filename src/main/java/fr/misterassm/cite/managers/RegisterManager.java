package fr.misterassm.cite.managers;

import com.earth2me.essentials.api.UserDoesNotExistException;
import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import fr.misterassm.cite.Cite;
import fr.misterassm.cite.commands.CiteCommand;
import fr.misterassm.cite.enums.CiteEnum;
import fr.misterassm.cite.listeners.*;
import fr.misterassm.cite.managers.fight.Fight;
import fr.misterassm.cite.managers.player.PlayerCite;
import fr.misterassm.cite.utils.Formatter;
import fr.misterassm.cite.utils.scoreboard.FastBoard;
import me.clip.placeholderapi.PlaceholderAPI;
import me.clip.placeholderapi.PlaceholderHook;
import net.ess3.api.Economy;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;

public class RegisterManager {

    private Cite instance = Cite.getInstance();

    public void register() {
        /**
         * UTILS
         */

        Bukkit.getServer().getScheduler().runTaskTimer(Cite.getInstance(), () -> {
            for (FastBoard board : Cite.getInstance().getBoards().values()) {
                try {
                    updateBoard(board);
                } catch (UserDoesNotExistException e) {
                    e.printStackTrace();
                }
            }
        }, 0, 20);

        this.registerPlaceHolders();

        /**
         * LISTENERS
         */

        PluginManager pluginManager = Bukkit.getPluginManager();

        pluginManager.registerEvents(new PlayerJoin(), instance);
        pluginManager.registerEvents(new PlayerDeath(), instance);
        pluginManager.registerEvents(new PlayerQuit(), instance);
        pluginManager.registerEvents(new PlayerCommand(), instance);
        pluginManager.registerEvents(new OnDamage(), instance);

        /**
         * COMMANDS
         */

        instance.getCommand("cite").setExecutor(new CiteCommand());
    }


    private void updateBoard(FastBoard board) throws UserDoesNotExistException {

        if (Fight.getInstance().hasFight(board.getPlayer())) {
            board.updateLines(
                    "§1",
                    " §e§lCITEES",
                    "§4",
                    "  §9Athènes §7: " + (String.valueOf(Fight.getInstance().getPoints(CiteEnum.ATHENES)) == null ? -1 : Fight.getInstance().getPoints(CiteEnum.ATHENES)),
                    "  §6Alexandrie §7: " + (String.valueOf(Fight.getInstance().getPoints(CiteEnum.ALEXANDRIE)) == null ? -1 : Fight.getInstance().getPoints(CiteEnum.ALEXANDRIE)),
                    "§2",
                    "§emc.pvp-warcraft.net"
            );
        } else {
            FPlayer faction = FPlayers.getInstance().getByPlayer(board.getPlayer());
            String cite = (PlayerCite.getInstance().getCiteByPlayer(board.getPlayer().getName()) == null) ? "§7-" : PlayerCite.getInstance().getCiteByPlayer(board.getPlayer().getName()).getCite();;

            board.updateLines(
                    "§1",
                    " §e§lPROFIL",
                    "    Money: §b" + Formatter.numberFormat(Economy.getMoneyExact(board.getPlayer().getName()).longValue()),
                    "    Power: §a" + faction.getPowerRounded(),
                    "    Cité: §9" + cite,
                    "§2",
                    " §e§lFACTIONS",
                    "    Nom: " + faction.getFaction().getTag(),
                    "    Power: " + (faction.getFaction().getTag().equalsIgnoreCase("§aWilderness") ? "§7-" : "§a" + faction.getFaction().getPowerRounded()),
                    "    Connecté(s): §b" + (faction.getFaction().getTag().equalsIgnoreCase("§aWilderness") ? "§7-": "§b" + faction.getFaction().getOnlinePlayers().size()),
                    "§3",
                    "§7" + Bukkit.getOnlinePlayers().size() + " joueur(s)",
                    "§emc.pvp-warcraft.net"
            );
        }
    }

    private void registerPlaceHolders() {
        PlaceholderAPI.registerPlaceholderHook("warcraft", new PlaceholderHook() {
            @Override
            public String onPlaceholderRequest(Player player, String s) {
                if (player == null) {
                    return null;
                }

                if (s.equalsIgnoreCase("player")) {
                    return CiteEnum.getCiteByName(PlayerCite.getInstance().getCiteByPlayer(player.getName()).getCite()).getName();
                }

                if (s.equalsIgnoreCase("kills")) {
                    return String.valueOf(PlayerCite.getInstance().getKills(player.getName()));
                }

                if (s.equalsIgnoreCase("deaths")) {
                    return String.valueOf(PlayerCite.getInstance().getDeaths(player.getName()));
                }

                if (s.equalsIgnoreCase("ratio")) {
                    int kills = PlayerCite.getInstance().getKills(player.getName());
                    int deaths = PlayerCite.getInstance().getDeaths(player.getName());
                    double kd = (deaths == 0 ? kills : kills / deaths);

                    return String.valueOf(kd);
                }

                return null;
            }
        });
    }

}
