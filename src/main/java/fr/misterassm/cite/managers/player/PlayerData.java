package fr.misterassm.cite.managers.player;

public class PlayerData {

    private String player;
    private String cite;
    private int kills;
    private int deaths;

    public PlayerData(String player, String cite) {
        this.player = player;
        this.cite = cite;
        this.kills = 0;
        this.deaths = 0;
    }

    /**
     * GETTER
     */

    public String getCite() { return cite; }
    public String getPlayer() { return player; }
    public int getKills() { return kills; }
    public int getDeaths() { return deaths; }

    /**
     * SETTER
     */

    public void setCite(String cite) { this.cite = cite; }
    public void setKills(int kills) { this.kills = kills; }
    public void setDeaths(int deaths) { this.deaths = deaths; }

}
