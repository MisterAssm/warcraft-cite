package fr.misterassm.cite.managers.player.exceptions;

public class PlayerAlreadyInCiteException extends Exception {

    public PlayerAlreadyInCiteException(String message) {
        super(message);
    }
}
