package fr.misterassm.cite.managers.player;

import fr.misterassm.cite.enums.CiteEnum;
import fr.misterassm.cite.managers.player.exceptions.PlayerAlreadyInCiteException;
import fr.misterassm.cite.managers.player.exceptions.PlayerAlreadySameCiteException;

import java.util.List;

public abstract class PlayerCite {

    protected static PlayerCite instance;

    public PlayerCite() {
        instance = this;
    }

    public static PlayerCite getInstance() { return instance; }

    public abstract void saveFile();

    public abstract void loadFile();

    public abstract PlayerData getCite(CiteEnum citeEnum);

    public abstract PlayerData getCiteByName(String name);

    public abstract PlayerData getCiteByPlayer(String name);

    public abstract boolean hasPlayerCite(String player);

    public abstract int getCiteNumber(CiteEnum citeEnum);

    public abstract boolean canJoinCite(CiteEnum citeEnum, CiteEnum citeEnum1);

    public abstract void addPlayerToCite(CiteEnum citeEnum, String player) throws PlayerAlreadyInCiteException;

    public abstract void changePlayerCite(CiteEnum citeEnum, String player) throws PlayerAlreadySameCiteException;

    public abstract void addKill(String player);

    public abstract void addKill(String player, int number);

    public abstract void addDeath(String player);

    public abstract void addDeath(String player, int number);

    public abstract int getKills(String player);

    public abstract int getDeaths(String player);

    public abstract List<PlayerData> getPlayerData();

}
