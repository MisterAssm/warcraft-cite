package fr.misterassm.cite.managers.player.exceptions;

public class PlayerAlreadySameCiteException extends Exception {

    public PlayerAlreadySameCiteException(String message) {
        super(message);
    }
}
