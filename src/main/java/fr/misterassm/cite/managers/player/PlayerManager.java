package fr.misterassm.cite.managers.player;

import fr.misterassm.cite.Cite;
import fr.misterassm.cite.enums.CiteEnum;
import fr.misterassm.cite.managers.player.exceptions.PlayerAlreadyInCiteException;
import fr.misterassm.cite.managers.player.exceptions.PlayerAlreadySameCiteException;
import fr.misterassm.cite.utils.json.JSONReader;
import fr.misterassm.cite.utils.json.JSONWriter;
import fr.misterassm.cite.utils.json.utils.JsonUtil;
import org.bukkit.Bukkit;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PlayerManager extends PlayerCite {

    private JsonUtil jsonUtil;
    private File file;
    private List<PlayerData> playerData;

    public PlayerManager() {
        this.jsonUtil = new JsonUtil();
        this.playerData = new ArrayList<>();
        this.file = new File(Cite.getInstance().getDataFolder(), "player.json");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void saveFile() {
        JSONArray array = new JSONArray();
        for (PlayerData data : this.playerData) array.put(jsonUtil.serialize(data));
        try (JSONWriter writer = new JSONWriter(file)) {
            writer.write(array);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void loadFile() {
        try {
            JSONReader reader = new JSONReader(file);
            JSONArray array = reader.toJSONArray();
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                this.playerData.add(jsonUtil.deserialize(object, PlayerData.class));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public PlayerData getCite(CiteEnum citeEnum) {
        for (PlayerData data : playerData) {
            if (data.getCite().equalsIgnoreCase(citeEnum.getFile())) return data;
        }

        return null;
    }

    @Override
    public PlayerData getCiteByName(String name) {
        for (PlayerData data : playerData) {
            if (data.getCite().equalsIgnoreCase(name)) return data;
        }

        return null;
    }

    @Override
    public PlayerData getCiteByPlayer(String name) {
        for (PlayerData data : playerData) {
            if (data.getPlayer().equalsIgnoreCase(name)) return data;
        }

        return null;
    }

    @Override
    public boolean hasPlayerCite(String player) {
        for (PlayerData data : playerData) {
            if (data.getPlayer().equalsIgnoreCase(player)) return true;
        }

        return false;
    }

    @Override
    public int getCiteNumber(CiteEnum citeEnum) {
        int number = 0;
        for (PlayerData data : playerData) {
            if (data.getCite().equalsIgnoreCase(citeEnum.getFile())) number = number + 1;
        }

        return number;
    }

    @Override
    public boolean canJoinCite(CiteEnum citeEnum, CiteEnum citeEnum1) {
        return getCiteNumber(citeEnum) - getCiteNumber(citeEnum1) <= 10;
    }

    @Override
    public void addPlayerToCite(CiteEnum citeEnum, String player) throws PlayerAlreadyInCiteException {
        if (hasPlayerCite(player))
            throw new  PlayerAlreadyInCiteException("Player can't already be in a player");

        this.playerData.add(new PlayerData(player, citeEnum.getFile()));
        Bukkit.dispatchCommand(Bukkit.getServer().getConsoleSender(), "lp user " + player +" permission set " + citeEnum.getGroup());
        System.out.println(getCiteByPlayer(player).getCite());
    }

    /**
     *
     * @param citeEnum the new player of the player.
     * @param player
     * @throws PlayerAlreadySameCiteException
     */
    @Override
    public void changePlayerCite(CiteEnum citeEnum, String player) throws PlayerAlreadySameCiteException {
        if (getCiteByPlayer(player).equals(getCiteByName(citeEnum.getFile())))
            throw new PlayerAlreadySameCiteException("Player can't already be in the same");

        Bukkit.dispatchCommand(Bukkit.getServer().getConsoleSender(), "lp user " + player +" permission unset " + CiteEnum.getCiteByName(getCiteByPlayer(player).getCite()).getGroup());
        playerData.remove(getCiteByPlayer(player));
        try {
            addPlayerToCite(citeEnum, player);
            Bukkit.dispatchCommand(Bukkit.getServer().getConsoleSender(), "lp user " + player +" permission set " + citeEnum.getGroup());
        } catch (PlayerAlreadyInCiteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addKill(String player) {
        getCiteByPlayer(player).setKills(
                getCiteByPlayer(player).getKills() + 1
        );
    }

    @Override
    public void addKill(String player, int number) {
        getCiteByPlayer(player).setKills(
                getCiteByPlayer(player).getKills() + number
        );
    }

    @Override
    public void addDeath(String player) {
        getCiteByPlayer(player).setDeaths(
                getCiteByPlayer(player).getDeaths() + 1
        );
    }

    @Override
    public void addDeath(String player, int number) {
        getCiteByPlayer(player).setDeaths(
                getCiteByPlayer(player).getDeaths() + number
        );
    }

    @Override
    public int getKills(String player) {
        return getCiteByPlayer(player).getKills();
    }

    @Override
    public int getDeaths(String player) {
        return getCiteByPlayer(player).getDeaths();
    }

    @Override
    public List<PlayerData> getPlayerData() {
        return this.playerData;
    }

}
