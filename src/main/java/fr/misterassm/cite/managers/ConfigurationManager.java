package fr.misterassm.cite.managers;

import fr.misterassm.cite.Cite;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

@SuppressWarnings("ResultOfMethodCallIgnored")
public class ConfigurationManager {

    private static FileConfiguration fileConfiguration;

    public void createConfigurationFile() {

        fileConfiguration = YamlConfiguration.loadConfiguration(getFile());
        File file = new File(Cite.getInstance().getDataFolder(), "configuration.yml");

        try {
            if (!file.exists())
                file.createNewFile();


            verificationLine("configuration.price", 25000000);
            verificationLine("configuration.spawn.world", "world");
            verificationLine("configuration.spawn.X", 0);
            verificationLine("configuration.spawn.Y", 200);
            verificationLine("configuration.spawn.Z", 0);
            verificationLine("configuration.fight.world", "world");
            verificationLine("configuration.athenes.X", 0);
            verificationLine("configuration.athenes.Y", 200);
            verificationLine("configuration.athenes.Z", 0);
            verificationLine("configuration.alexandrie.X", 50);
            verificationLine("configuration.alexandrie.Y", 200);
            verificationLine("configuration.alexandrie.Z", 50);
            verificationLine("configuration.wait.world", "world");
            verificationLine("configuration.wait.X", 20);
            verificationLine("configuration.wait.Y", 200);
            verificationLine("configuration.wait.Z", 20);

            verificationLine("lang.noPermission", "&cVous n'avez pas la permission d'éxecuter cette commande.");
            verificationLine("lang.help", "&7Help");
            verificationLine("lang.citeinfo", "&7----- &eSatistiques &7-----\\n" +
                    "&8» &e{cite.kills} kills\\n" +
                    "&8» &e{cite.deaths} morts\\n" +
                    "&8» &e{cite.victory} victoires\\n" +
                    "&8» &e{cite.defeat} défaites\\n" +
                    "&8» &e{player.kills} kills &7(Personnel)\\n" +
                    "&8» &e{player.deaths} morts &7(personnel)\\n");

            verificationLine("fight.help", "&7Fight help");
            verificationLine("fight.broadcast1", "&7Tu fera le message de blabla pouki\\n\\n");
            verificationLine("fight.cliquableJoin", "&a[Partir sur le front]");
            verificationLine("fight.broadcast2", "\\n\\nEn mode tu verra ça permet de structure ton texte tranquille");
            verificationLine("fight.forceStop", "&cLe combat a été annulé.");
            verificationLine("fight.joinWaiting", "&aVous venez de rejoindre la salle d'attente.");
            verificationLine("fight.winCommand", "kit vipplus {player}");
            verificationLine("fight.win", "&eVictoire de l'équipe {cite} &e!");
            verificationLine("fight.join.notWaitingPhase", "&cIl n'y a pas de combat en phase d'attente");
            verificationLine("fight.join.alreadyInFight", "&cVous êtes déjà dans la liste des combattants.");
            verificationLine("fight.tryCommand", "&cVous ne pouvez pas executer de commande en combat de cité !");
            verificationLine("fight.admin.startingFight", "&aLancement du combat inter-cité !");
            verificationLine("fight.admin.alreadyInFight", "&cVous ne pouvez pas lancer un fight si un fight est déjà en cours.");
            verificationLine("fight.admin.stopNotStart", "&cVous ne pouvez pas stopper un fight si un aucun fight n'est en cours.");

            verificationLine("fight.wincommand", "kit vipplus {player}");

            verificationLine("commands.join.invalidArgument", "&cVeuillez renseigner une cité !");

            verificationLine("inventory.choose.signinfo", "&6Ligne 1\\n&aLigne 2");
            verificationLine("inventory.switch.name", "&7Voulez-vous rejoindre la cité &a{cite} &7?");
            verificationLine("inventory.switch.notEnoughMoney", "§cVous n'avez pas l'argent nécessaire pour changer de cité !");
            verificationLine("inventory.switch.accept", "&a&lAccepter &8&l[&a&l✔&8&l]");
            verificationLine("inventory.switch.refuse", "&c&lAnnuler &8&l[&c&l✘&8&l]");
            verificationLine("inventory.switch.canceledMessage", "&cVotre demande a été annulé.");
            verificationLine("inventory.switch.processSuccessful", "&aVous venez de rejoindre la cité {cite} !");

            verificationLine("inventory.join.name", "&7Voulez-vous rejoindre la cité {cite} &7?");
            verificationLine("inventory.join.accept", "&a&lAccepter &8&l[&a&l✔&8&l]");
            verificationLine("inventory.join.refuse", "&c&lAnnuler &8&l[&c&l✘&8&l]");
            verificationLine("inventory.join.canceledMessage", "&cVotre demande a été annulé.");
            verificationLine("inventory.join.notEnoughMoney", "§cVous n'avez pas l'argent nécessaire pour changer de cité !");
            verificationLine("inventory.join.processSuccessful", "&aVous venez de rejoindre la cité {cite} !");
            verificationLine("inventory.join.alreadyInSameCite", "&cVous êtes déjà dans la cité {cite} !");

            fileConfiguration.save(getFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void verificationLine(String path, Object value) { if (!fileConfiguration.isSet(path)) { fileConfiguration.set(path, value); } }
    private File getFile() { return new File(Cite.getInstance().getDataFolder(), "configuration" + ".yml"); }

    public static String getMessage(String path) {
        return String.valueOf(fileConfiguration.get(path)).replace("&", "§").replaceAll("\\\\n", "\n");
    }
}
