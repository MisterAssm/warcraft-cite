package fr.misterassm.cite.managers.fight;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class FightData {

    private Player player;
    private ItemStack[] inventory;
    private ItemStack helmet;
    private ItemStack chestplate;
    private ItemStack leggings;
    private ItemStack boots;

    public FightData(Player player, PlayerInventory inventory) {
        this.player = player;
        this.inventory = inventory.getContents();
        this.helmet = inventory.getHelmet();
        this.chestplate = inventory.getChestplate();
        this.leggings = inventory.getLeggings();
        this.boots = inventory.getBoots();
    }

    public Player getPlayer() { return player; }
    public ItemStack[] getInventory() { return inventory; }
    public ItemStack getHelmet() { return helmet; }
    public ItemStack getChestplate() { return chestplate; }
    public ItemStack getLeggings() { return leggings; }
    public ItemStack getBoots() { return boots; }

}
