package fr.misterassm.cite.managers.fight.exceptions;

public class FightNotStartedException extends Exception {

    public FightNotStartedException(String message) {
        super(message);
    }

}
