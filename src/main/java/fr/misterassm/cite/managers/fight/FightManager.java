package fr.misterassm.cite.managers.fight;

import fr.misterassm.cite.Cite;
import fr.misterassm.cite.enums.CiteEnum;
import fr.misterassm.cite.enums.FightState;
import fr.misterassm.cite.managers.ConfigurationManager;
import fr.misterassm.cite.managers.cite.CiteData;
import fr.misterassm.cite.managers.cite.CiteManager;
import fr.misterassm.cite.managers.fight.exceptions.AlreadyInFightException;
import fr.misterassm.cite.managers.fight.exceptions.FightAlreadyStartedException;
import fr.misterassm.cite.managers.fight.exceptions.FightNotStartedException;
import fr.misterassm.cite.managers.fight.exceptions.PlayerNotFoundException;
import fr.misterassm.cite.managers.player.PlayerCite;
import fr.misterassm.cite.utils.Title;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class FightManager extends Fight {

    private List<FightData> fightData = new ArrayList<>();
    private Map<CiteEnum, AtomicInteger> number = new HashMap<>();

    @Override
    public void startWaiting() throws FightAlreadyStartedException {
        if (!Cite.getInstance().getFightState().equals(FightState.NOT_START))
            throw new FightAlreadyStartedException("Can't start a fight already begun.");

        number.put(CiteEnum.ATHENES, new AtomicInteger(100));
        number.put(CiteEnum.ALEXANDRIE, new AtomicInteger(100));

        TextComponent message = new TextComponent(ConfigurationManager.getMessage("fight.broadcast1"));
        TextComponent cliquableMessage = new TextComponent(ConfigurationManager.getMessage("fight.cliquableJoin"));
        cliquableMessage.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/cite fight join"));
        TextComponent message1 = new TextComponent(ConfigurationManager.getMessage("fight.broadcast2"));

        for (Player player : Bukkit.getOnlinePlayers()) {
            player.spigot().sendMessage(message, cliquableMessage, message1);
        }

        Cite.getInstance().setFightState(FightState.WAITING);
    }

    @Override
    public void startFight() {

        for (FightData data : this.fightData) {
            Player player = data.getPlayer();

            new Timer().schedule(
                    new TimerTask() {
                        @Override
                        public void run() {
                            Title.sendTitle(player, 0, 20, 10, "§7Lancement dans..", "§a5");

                            new Timer().schedule(
                                    new TimerTask() {
                                        @Override
                                        public void run() {
                                            Title.sendTitle(player, 0, 20, 10, "§7Lancement dans..", "§a4");

                                            new Timer().schedule(
                                                    new TimerTask() {
                                                        @Override
                                                        public void run() {
                                                            Title.sendTitle(player, 0, 20, 10, "§7Lancement dans..", "§63");

                                                            new Timer().schedule(
                                                                    new TimerTask() {
                                                                        @Override
                                                                        public void run() {
                                                                            Title.sendTitle(player, 0, 20, 10, "§7Lancement dans..", "§c2");

                                                                            new Timer().schedule(
                                                                                    new TimerTask() {
                                                                                        @Override
                                                                                        public void run() {
                                                                                            Title.sendTitle(player, 0, 20, 10, "§7Lancement dans..", "§c1");

                                                                                            new Timer().schedule(
                                                                                                    new TimerTask() {
                                                                                                        @Override
                                                                                                        public void run() {
                                                                                                            Title.sendTitle(player, 0, 20, 10, "§aBon courage !", "§7Que le meilleur gagne");

                                                                                                            player.getInventory().clear();
                                                                                                            player.getInventory().setHelmet(new ItemStack(Material.AIR, 1));
                                                                                                            player.getInventory().setChestplate(new ItemStack(Material.AIR, 1));
                                                                                                            player.getInventory().setLeggings(new ItemStack(Material.AIR, 1));
                                                                                                            player.getInventory().setBoots(new ItemStack(Material.AIR, 1));

                                                                                                            Cite.getInstance().setFightState(FightState.FIGHT);
                                                                                                            teleportPlayer();

                                                                                                            Bukkit.dispatchCommand(Bukkit.getServer().getConsoleSender(), ConfigurationManager.getMessage("fight.wincommand").replaceAll("\\{player}", player.getName()));
                                                                                                        }}, 1000);
                                                                                        }}, 1000);
                                                                        }}, 1000);
                                                        }}, 1000);
                                        }}, 1000);
                        }}, 1000);
        }
    }

    @Override
    public void teleportPlayer() {

        for (FightData data : fightData) {

            if (PlayerCite.getInstance().getCiteByPlayer(data.getPlayer().getName()).getCite().equalsIgnoreCase(CiteEnum.ATHENES.getFile())) {
                data.getPlayer().teleport(new Location(Bukkit.getWorld(ConfigurationManager.getMessage("configuration.fight.world")),
                        Integer.parseInt(ConfigurationManager.getMessage("configuration.athenes.X")),
                        Integer.parseInt(ConfigurationManager.getMessage("configuration.athenes.Y")),
                        Integer.parseInt(ConfigurationManager.getMessage("configuration.athenes.Z"))));
            } else {
                data.getPlayer().teleport(new Location(Bukkit.getWorld(ConfigurationManager.getMessage("configuration.fight.world")),
                        Integer.parseInt(ConfigurationManager.getMessage("configuration.alexandrie.X")),
                        Integer.parseInt(ConfigurationManager.getMessage("configuration.alexandrie.Y")),
                        Integer.parseInt(ConfigurationManager.getMessage("configuration.alexandrie.Z"))));
            }

        }

    }

    @Override
    public void teleportPlayer(Player player) {

        if (PlayerCite.getInstance().getCiteByPlayer(player.getName()).getCite().equalsIgnoreCase(CiteEnum.ATHENES.getFile())) {
            player.teleport(new Location(Bukkit.getWorld(ConfigurationManager.getMessage("configuration.fight.world")),
                    Integer.parseInt(ConfigurationManager.getMessage("configuration.athenes.X")),
                    Integer.parseInt(ConfigurationManager.getMessage("configuration.athenes.Y")),
                    Integer.parseInt(ConfigurationManager.getMessage("configuration.athenes.Z"))));
        } else {
            player.teleport(new Location(Bukkit.getWorld(ConfigurationManager.getMessage("configuration.fight.world")),
                    Integer.parseInt(ConfigurationManager.getMessage("configuration.alexandrie.X")),
                    Integer.parseInt(ConfigurationManager.getMessage("configuration.alexandrie.Y")),
                    Integer.parseInt(ConfigurationManager.getMessage("configuration.alexandrie.Z"))));
        }

    }

    @Override
    public void teleportSpawn() {

        for (FightData data : fightData) {
            data.getPlayer().teleport(new Location(Bukkit.getWorld(ConfigurationManager.getMessage("configuration.spawn.world")),
                    Integer.parseInt(ConfigurationManager.getMessage("configuration.spawn.X")),
                    Integer.parseInt(ConfigurationManager.getMessage("configuration.spawn.Y")),
                    Integer.parseInt(ConfigurationManager.getMessage("configuration.spawn.Z"))));
        }

    }

    @Override
    public void teleportSpawn(Player player) {

        player.teleport(new Location(Bukkit.getWorld(ConfigurationManager.getMessage("configuration.spawn.world")),
                Integer.parseInt(ConfigurationManager.getMessage("configuration.spawn.X")),
                Integer.parseInt(ConfigurationManager.getMessage("configuration.spawn.Y")),
                Integer.parseInt(ConfigurationManager.getMessage("configuration.spawn.Z"))));

    }

    @Override
    public void teleportWait(Player player) {

        player.teleport(new Location(Bukkit.getWorld(ConfigurationManager.getMessage("configuration.wait.world")),
                Integer.parseInt(ConfigurationManager.getMessage("configuration.wait.X")),
                Integer.parseInt(ConfigurationManager.getMessage("configuration.wait.Y")),
                Integer.parseInt(ConfigurationManager.getMessage("configuration.wait.Z"))));

    }

    @Override
    public void forceStop() throws FightNotStartedException {
        if (Cite.getInstance().getFightState().equals(FightState.NOT_START))
            throw new FightNotStartedException("Can't stop a fight not already started");

        for (int i = 0; i < fightData.size(); i++) {
            Player player = fightData.get(i).getPlayer();

            teleportSpawn(player);
            player.getInventory().clear();
            player.getInventory().setHelmet(new ItemStack(Material.AIR, 1));
            player.getInventory().setChestplate(new ItemStack(Material.AIR, 1));
            player.getInventory().setLeggings(new ItemStack(Material.AIR, 1));
            player.getInventory().setBoots(new ItemStack(Material.AIR, 1));

            FightData data = getFightByPlayer(player);

            for (ItemStack item : data.getInventory()) {
                if (item == null) continue;
                player.getInventory().addItem(item);
            }

            player.getInventory().setHelmet(data.getHelmet());
            player.getInventory().setChestplate(data.getChestplate());
            player.getInventory().setLeggings(data.getLeggings());
            player.getInventory().setBoots(data.getBoots());
            fightData.remove(getFightByPlayer(player));

            player.sendMessage(ConfigurationManager.getMessage("fight.forceStop"));
        }

        number.remove(CiteEnum.ATHENES);
        number.remove(CiteEnum.ALEXANDRIE);

        Cite.getInstance().setFightState(FightState.NOT_START);
    }

    @Override
    public void playerQuit(Player player) throws FightNotStartedException {
        if (!Cite.getInstance().getFightState().equals(FightState.FIGHT))
            throw new FightNotStartedException("Can't stop a fight not already started");

        removePoints(CiteEnum.getCiteByName(PlayerCite.getInstance().getCiteByPlayer(player.getName()).getCite()));

        player.getInventory().clear();
        player.getInventory().setHelmet(new ItemStack(Material.AIR, 1));
        player.getInventory().setChestplate(new ItemStack(Material.AIR, 1));
        player.getInventory().setLeggings(new ItemStack(Material.AIR, 1));
        player.getInventory().setBoots(new ItemStack(Material.AIR, 1));

        for (ItemStack item : player.getInventory().getContents()) {
            if (item == null) continue;
            player.getInventory().addItem(item);
        }

        player.getInventory().setHelmet(player.getInventory().getHelmet());
        player.getInventory().setChestplate(player.getInventory().getChestplate());
        player.getInventory().setLeggings(player.getInventory().getLeggings());
        player.getInventory().setBoots(player.getInventory().getBoots());
    }

    @Override
    public boolean hasFight(Player player) {
        for (FightData data : fightData) {
            if (data.getPlayer().equals(player)) return true;
        }

        return false;
    }

    @Override
    public FightData getFightByPlayer(Player player) {
        for (FightData data : fightData) {
            if (data.getPlayer().equals(player)) return data;
        }

        return null;
    }

    @Override
    public void addPlayer(Player player) throws AlreadyInFightException, FightNotStartedException {
        if (!Cite.getInstance().getFightState().equals(FightState.WAITING))
            throw new FightNotStartedException("Can't join a fight when isn't in waiting phase.");
        if (hasFight(player))
            throw new AlreadyInFightException("Player can't already be in fight");

        fightData.add(new FightData(player, player.getInventory()));

        player.getInventory().clear();
        player.getInventory().setHelmet(new ItemStack(Material.AIR, 1));
        player.getInventory().setChestplate(new ItemStack(Material.AIR, 1));
        player.getInventory().setLeggings(new ItemStack(Material.AIR, 1));
        player.getInventory().setBoots(new ItemStack(Material.AIR, 1));

        teleportWait(player);
        player.sendMessage(ConfigurationManager.getMessage("fight.joinWaiting"));
    }

    @Override
    public void removePlayer(Player player) throws PlayerNotFoundException {
        if (!hasFight(player))
            throw new PlayerNotFoundException("Can't remove a not found player.");

        teleportSpawn(player);
        player.getInventory().clear();
        player.getInventory().setHelmet(new ItemStack(Material.AIR, 1));
        player.getInventory().setChestplate(new ItemStack(Material.AIR, 1));
        player.getInventory().setLeggings(new ItemStack(Material.AIR, 1));
        player.getInventory().setBoots(new ItemStack(Material.AIR, 1));

        FightData data = getFightByPlayer(player);

        for (ItemStack item : data.getInventory()) {
            if (item == null) continue;
            player.getInventory().addItem(item);
        }

        player.getInventory().setHelmet(data.getHelmet());
        player.getInventory().setChestplate(data.getChestplate());
        player.getInventory().setLeggings(data.getLeggings());
        player.getInventory().setBoots(data.getBoots());
        fightData.remove(getFightByPlayer(player));
    }

    @Override
    public void stopFight(CiteData winner, CiteData looser) throws FightNotStartedException {
        if (!Cite.getInstance().getFightState().equals(FightState.FIGHT))
            throw new FightNotStartedException("Can't stop a fight not already started");

        CiteManager.getInstance().addVictory(winner);
        CiteManager.getInstance().addDefeat(looser);
        number.remove(CiteEnum.ATHENES);
        number.remove(CiteEnum.ALEXANDRIE);

        for (int i = 0; i < fightData.size(); i++) {
            Player player = fightData.get(i).getPlayer();

            teleportSpawn(player);
            player.getInventory().clear();
            player.getInventory().setHelmet(new ItemStack(Material.AIR, 1));
            player.getInventory().setChestplate(new ItemStack(Material.AIR, 1));
            player.getInventory().setLeggings(new ItemStack(Material.AIR, 1));
            player.getInventory().setBoots(new ItemStack(Material.AIR, 1));

            FightData data = getFightByPlayer(player);

            for (ItemStack item : data.getInventory()) {
                if (item == null) continue;
                player.getInventory().addItem(item);
            }

            player.getInventory().setHelmet(data.getHelmet());
            player.getInventory().setChestplate(data.getChestplate());
            player.getInventory().setLeggings(data.getLeggings());
            player.getInventory().setBoots(data.getBoots());
            fightData.remove(getFightByPlayer(player));

            player.sendMessage(ConfigurationManager.getMessage("fight.forceStop"));

            if (PlayerCite.getInstance().getCiteByPlayer(player.getName()).getCite().equals(CiteManager.getInstance().getCiteByName(winner.getCite()).getCite())) {
                Bukkit.dispatchCommand(Bukkit.getServer().getConsoleSender(), ConfigurationManager.getMessage("fight.winCommand").replaceAll("\\{player}", player.getName()));
            }
        }

        for (Player player : Bukkit.getOnlinePlayers()) {
            player.sendMessage(ConfigurationManager.getMessage("fight.win").replaceAll("\\{cite}", CiteEnum.getCiteByName(winner.getCite()).getName()));
        }
        Cite.getInstance().setFightState(FightState.NOT_START);
    }

    @Override
    public boolean isInFight(String player) {
        for (FightData data : fightData) {
            if (data.getPlayer().getName().equalsIgnoreCase(player)) return true;
        }

        return false;
    }

    @Override
    public int getPoints(CiteEnum citeEnum) {
        return (number.get(citeEnum).get());
    }

    @Override
    public void removePoints(CiteEnum citeEnum) throws FightNotStartedException {
        number.get(citeEnum).set(number.get(citeEnum).get() - 1);

        if (number.get(citeEnum).get() == 0) {

            CiteEnum winner = (citeEnum.equals(CiteEnum.ATHENES) ? CiteEnum.ALEXANDRIE : CiteEnum.ATHENES);

            stopFight(CiteManager.getInstance().getCiteByName(winner.getFile()), CiteManager.getInstance().getCiteByName(citeEnum.getFile()));

        }
    }
}
