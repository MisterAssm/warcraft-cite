package fr.misterassm.cite.managers.fight.exceptions;

public class AlreadyInFightException extends Exception {

    public AlreadyInFightException(String message) {
        super(message);
    }

}
