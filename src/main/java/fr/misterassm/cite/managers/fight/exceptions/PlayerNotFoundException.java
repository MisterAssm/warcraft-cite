package fr.misterassm.cite.managers.fight.exceptions;

public class PlayerNotFoundException extends Exception {

    public PlayerNotFoundException(String message) {
        super(message);
    }

}
