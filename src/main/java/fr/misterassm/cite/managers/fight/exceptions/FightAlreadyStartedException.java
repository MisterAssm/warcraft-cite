package fr.misterassm.cite.managers.fight.exceptions;

public class FightAlreadyStartedException extends Exception {

    public FightAlreadyStartedException(String message) {
        super(message);
    }

}
