package fr.misterassm.cite.managers.fight;

import fr.misterassm.cite.enums.CiteEnum;
import fr.misterassm.cite.managers.cite.CiteData;
import fr.misterassm.cite.managers.fight.exceptions.AlreadyInFightException;
import fr.misterassm.cite.managers.fight.exceptions.FightAlreadyStartedException;
import fr.misterassm.cite.managers.fight.exceptions.FightNotStartedException;
import fr.misterassm.cite.managers.fight.exceptions.PlayerNotFoundException;
import org.bukkit.entity.Player;

public abstract class Fight {

    protected static Fight instance;

    public Fight() {
        instance = this;
    }

    public static Fight getInstance() { return instance; }

    public abstract void startWaiting() throws FightAlreadyStartedException;

    public abstract void startFight();

    public abstract void teleportPlayer();

    public abstract void teleportPlayer(Player player);

    public abstract void teleportSpawn();

    public abstract void teleportSpawn(Player player);

    public abstract void teleportWait(Player player);

    public abstract void forceStop() throws FightNotStartedException;

    public abstract void playerQuit(Player player) throws FightNotStartedException;

    public abstract boolean hasFight(Player player);

    public abstract FightData getFightByPlayer(Player player);

    public abstract void addPlayer(Player player) throws AlreadyInFightException, FightNotStartedException;

    public abstract void removePlayer(Player player) throws PlayerNotFoundException;

    public abstract void stopFight(CiteData winner, CiteData looser) throws FightNotStartedException;

    public abstract boolean isInFight(String player);

    public abstract int getPoints(CiteEnum citeEnum);

    public abstract void removePoints(CiteEnum citeEnum) throws FightNotStartedException;

}
