package fr.misterassm.cite.managers.cite;

public class CiteData {

    public String cite;
    public int victory;
    public int defeat;

    public CiteData(String cite) {
        this.cite = cite;
        this.victory = 0;
        this.defeat = 0;
    }

    /**
     * GETTER
     */

    public String getCite() { return cite; }
    public int getVictory() { return victory; }
    public int getDefeat() { return defeat; }

    /**
     * SETTER
     */

    public void setCite(String cite) { this.cite = cite; }
    public void setVictory(int victory) { this.victory = victory; }
    public void setDefeat(int defeat) { this.defeat = defeat; }

}
