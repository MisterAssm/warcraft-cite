package fr.misterassm.cite.managers.cite;

import fr.misterassm.cite.enums.CiteEnum;

public abstract class AbstractCite {

    protected static AbstractCite instance;

    public AbstractCite() {
        instance = this;
    }

    public static AbstractCite getInstance() { return instance; }

    public abstract void saveFile();

    public abstract void loadFile();

    public abstract CiteData getCite(CiteEnum citeEnum);

    public abstract CiteData getCiteByName(String name);

    public abstract void addVictory(CiteData cite);

    public abstract void addDefeat(CiteData cite);

    public abstract int getVictory(CiteData cite);

    public abstract int getDefeat(CiteData cite);

    public abstract int getKills(CiteData cite);

    public abstract int getDeaths(CiteData cite);

}
