package fr.misterassm.cite.managers.cite;

import fr.misterassm.cite.Cite;
import fr.misterassm.cite.enums.CiteEnum;
import fr.misterassm.cite.managers.player.PlayerCite;
import fr.misterassm.cite.managers.player.PlayerData;
import fr.misterassm.cite.utils.json.JSONReader;
import fr.misterassm.cite.utils.json.JSONWriter;
import fr.misterassm.cite.utils.json.utils.JsonUtil;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CiteManager extends AbstractCite {

    private JsonUtil jsonUtil;
    private File file;
    private List<CiteData> citeData;

    public CiteManager() {
        this.jsonUtil = new JsonUtil();
        this.citeData = new ArrayList<>();
        this.file = new File(Cite.getInstance().getDataFolder(), "cite.json");
        if (!file.exists()) {
            try {
                file.createNewFile();
                for (int i = 0; i < CiteEnum.getCites().size(); i++) {
                    this.citeData.add(new CiteData(CiteEnum.getCites().get(i).getFile()));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void saveFile() {
        JSONArray array = new JSONArray();
        for (CiteData data : this.citeData) array.put(jsonUtil.serialize(data));
        try (JSONWriter writer = new JSONWriter(file)) {
            writer.write(array);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void loadFile() {
        try {
            JSONReader reader = new JSONReader(file);
            JSONArray array = reader.toJSONArray();
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                this.citeData.add(jsonUtil.deserialize(object, CiteData.class));
            }

            saveFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public CiteData getCite(CiteEnum citeEnum) {
        for (CiteData data : citeData) {
            if (data.getCite().equalsIgnoreCase(citeEnum.getFile())) return data;
        }

        return null;
    }

    @Override
    public CiteData getCiteByName(String name) {
        for (CiteData data : citeData) {
            if (data.getCite().equalsIgnoreCase(name)) return data;
        }

        return null;
    }

    @Override
    public void addVictory(CiteData cite) {
        cite.setVictory(
                cite.getVictory() + 1
        );
    }

    @Override
    public void addDefeat(CiteData cite) {
        cite.setDefeat(
                cite.getDefeat() + 1
        );
    }

    @Override
    public int getVictory(CiteData cite) {
        return cite.getVictory();
    }

    @Override
    public int getDefeat(CiteData cite) {
        return cite.getDefeat();
    }

    @Override
    public int getKills(CiteData cite) {
        int number = 0;
        for (PlayerData data : PlayerCite.getInstance().getPlayerData()) {
            if (data.getCite().equalsIgnoreCase(cite.getCite())) number = number + data.getKills();
        }

        return number;
    }

    @Override
    public int getDeaths(CiteData cite) {
        int number = 0;
        for (PlayerData data : PlayerCite.getInstance().getPlayerData()) {
            if (data.getCite().equalsIgnoreCase(cite.getCite())) number = number + data.getDeaths();
        }

        return number;
    }
}
