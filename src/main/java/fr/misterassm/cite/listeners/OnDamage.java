package fr.misterassm.cite.listeners;

import fr.misterassm.cite.Cite;
import fr.misterassm.cite.enums.FightState;
import fr.misterassm.cite.managers.player.PlayerCite;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class OnDamage implements Listener {

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent event) {
        if (Cite.getInstance().getFightState().equals(FightState.FIGHT)) {
            Entity victim = event.getEntity();
            Entity attacker = event.getDamager();

            if (victim instanceof Player) {
                if (attacker instanceof Player) {
                    if (PlayerCite.getInstance().getCiteByPlayer(victim.getName()).getCite()
                            .equals(PlayerCite.getInstance().getCiteByPlayer(attacker.getName()).getCite())) {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

}
