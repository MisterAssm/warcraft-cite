package fr.misterassm.cite.listeners;

import fr.misterassm.cite.Cite;
import fr.misterassm.cite.inventory.ChooseCiteInventory;
import fr.misterassm.cite.managers.player.PlayerCite;
import fr.misterassm.cite.utils.scoreboard.FastBoard;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoin implements Listener {

    @EventHandler
    public void playerJoin(PlayerJoinEvent event) {

        if (!PlayerCite.getInstance().hasPlayerCite(event.getPlayer().getName())) {
            Bukkit.getScheduler().runTask(Cite.getInstance(), () -> new ChooseCiteInventory().open(event.getPlayer()));
        }

        FastBoard board = new FastBoard(event.getPlayer());
        board.updateTitle("§6§6OP-FACTION");

        Cite.getInstance().getBoards().put(event.getPlayer().getName(), board);

    }

}
