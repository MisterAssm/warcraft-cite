package fr.misterassm.cite.listeners;

import fr.misterassm.cite.Cite;
import fr.misterassm.cite.managers.fight.Fight;
import fr.misterassm.cite.managers.fight.exceptions.FightNotStartedException;
import fr.misterassm.cite.utils.scoreboard.FastBoard;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuit implements Listener {

    @EventHandler
    public void playerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();

        FastBoard board = Cite.getInstance().getBoards().remove(player.getName());

        if (board != null) {
            board.delete();
        }

        if (Fight.getInstance().hasFight(player)) {

            Fight.getInstance().teleportSpawn(player);

            try {
                Fight.getInstance().playerQuit(player);
            } catch (FightNotStartedException e) {
                e.printStackTrace();
            }

        }
    }

}
