package fr.misterassm.cite.listeners;

import fr.misterassm.cite.Cite;
import fr.misterassm.cite.enums.CiteEnum;
import fr.misterassm.cite.managers.fight.Fight;
import fr.misterassm.cite.managers.fight.exceptions.FightNotStartedException;
import fr.misterassm.cite.managers.player.PlayerCite;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;

public class PlayerDeath implements Listener {

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        Player player = event.getEntity();

        if (Fight.getInstance().hasFight(player)) {

            try {
                Fight.getInstance().removePoints(CiteEnum.getCiteByName(PlayerCite.getInstance().getCiteByPlayer(player.getName()).getCite()));
            } catch (FightNotStartedException e) {
                e.printStackTrace();
            }

            event.getDrops().clear();

            player.getInventory().clear();
            player.getInventory().setHelmet(new ItemStack(Material.AIR, 1));
            player.getInventory().setChestplate(new ItemStack(Material.AIR, 1));
            player.getInventory().setLeggings(new ItemStack(Material.AIR, 1));
            player.getInventory().setBoots(new ItemStack(Material.AIR, 1));

            Bukkit.getScheduler().scheduleSyncDelayedTask(Cite.getInstance(), () -> {
                player.spigot().respawn();

                Bukkit.dispatchCommand(Bukkit.getServer().getConsoleSender(), "kit vipplus " + player.getName());

                Fight.getInstance().teleportPlayer(player);
            }, 1L);

        }
    }

}
