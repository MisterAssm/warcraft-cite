package fr.misterassm.cite.listeners;

import fr.misterassm.cite.managers.ConfigurationManager;
import fr.misterassm.cite.managers.fight.Fight;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class PlayerCommand implements Listener {

    @EventHandler
    public void onCommandPrompt(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();

        if (Fight.getInstance().hasFight(player) && !player.hasPermission("cite.admin")) {
            event.setCancelled(true);
            player.sendMessage(ConfigurationManager.getMessage("fight.tryCommand"));
        }
    }

}
