package fr.misterassm.cite.inventory;

import com.earth2me.essentials.api.Economy;
import com.earth2me.essentials.api.NoLoanPermittedException;
import com.earth2me.essentials.api.UserDoesNotExistException;
import fr.misterassm.cite.enums.CiteEnum;
import fr.misterassm.cite.managers.ConfigurationManager;
import fr.misterassm.cite.managers.player.PlayerCite;
import fr.misterassm.cite.managers.player.exceptions.PlayerAlreadySameCiteException;
import fr.misterassm.cite.utils.ItemBuilder;
import fr.misterassm.cite.utils.inventory.FastInv;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.math.BigDecimal;
import java.util.stream.IntStream;

public class SwitchInventory extends FastInv {

    public SwitchInventory(Player player) {
        super(45, ConfigurationManager.getMessage("inventory.switch.name").replaceAll("\\{cite}",
                (PlayerCite.getInstance().getCiteByPlayer(player.getName()).getCite().equalsIgnoreCase(CiteEnum.ATHENES.getFile()) ?
                        CiteEnum.ALEXANDRIE.getName() : CiteEnum.ATHENES.getName())));

        setItem(20, new ItemBuilder(Material.INK_SACK).setDyeColor(DyeColor.ORANGE)
                .setName(ConfigurationManager.getMessage("inventory.switch.refuse"))
                .toItemStack(), event -> {

            event.getWhoClicked().closeInventory();
            player.sendMessage(ConfigurationManager.getMessage("inventory.switch.canceledMessage"));
        });

        PlayerCite cite = PlayerCite.getInstance();

        String citeEnum;

        if (cite.getCiteByPlayer(player.getName()).getCite().equalsIgnoreCase(CiteEnum.ATHENES.getFile())) {
            citeEnum = CiteEnum.ALEXANDRIE.getFile();
        } else {
            citeEnum = CiteEnum.ATHENES.getFile();
        }

        setItem(24, new ItemBuilder(Material.INK_SACK).setDyeColor(DyeColor.PURPLE)
                .setName(ConfigurationManager.getMessage("inventory.switch.accept"))
                .toItemStack(), event -> {

            try {
                BigDecimal money = Economy.getMoneyExact(player.getName());

                if (money.intValue() < Integer.parseInt(ConfigurationManager.getMessage("configuration.price"))) {
                    event.getWhoClicked().closeInventory();
                    event.getWhoClicked().sendMessage(ConfigurationManager.getMessage("inventory.switch.notEnoughMoney"));
                } else {
                    Economy.substract(player.getName(), new BigDecimal(25000000));
                    PlayerCite.getInstance().changePlayerCite(CiteEnum.getCiteByName(citeEnum), event.getWhoClicked().getName());
                    event.getWhoClicked().closeInventory();
                    event.getWhoClicked().sendMessage(ConfigurationManager.getMessage("inventory.switch.processSuccessful").replaceAll("\\{cite}", citeEnum));
                }

            } catch (PlayerAlreadySameCiteException | UserDoesNotExistException | NoLoanPermittedException ignored) {
                event.getWhoClicked().sendMessage("§cUne erreur est survenue...");
            }
        });

        /**
         * setItems
         */
        setItems(this.getAroundCase(24), new ItemBuilder(Material.STAINED_GLASS_PANE).setDyeColor(DyeColor.GREEN).setName(" ").toItemStack());
        setItems(this.getAroundCase(20), new ItemBuilder(Material.STAINED_GLASS_PANE).setDyeColor(DyeColor.RED).setName(" ").toItemStack());

    }

    private int[] getAroundCase(int inventoryCase) {
        return IntStream.range(0, getInventory().getSize()).filter(i -> i == inventoryCase - 1 || i == inventoryCase - 8 || i == inventoryCase - 9 || i == inventoryCase - 10 || i == inventoryCase + 1 || i == inventoryCase + 8 || i == inventoryCase + 9 || i == inventoryCase + 10).toArray();
    }
}
