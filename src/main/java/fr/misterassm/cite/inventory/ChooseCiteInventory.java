package fr.misterassm.cite.inventory;

import fr.misterassm.cite.Cite;
import fr.misterassm.cite.enums.CiteEnum;
import fr.misterassm.cite.managers.ConfigurationManager;
import fr.misterassm.cite.managers.player.PlayerCite;
import fr.misterassm.cite.managers.player.exceptions.PlayerAlreadyInCiteException;
import fr.misterassm.cite.utils.Formatter;
import fr.misterassm.cite.utils.ItemBuilder;
import fr.misterassm.cite.utils.inventory.FastInv;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemStack;

public class ChooseCiteInventory extends FastInv {

    public ChooseCiteInventory() {
        super(45, "§7Choisissez votre §adestin§7.");

        setItem(20, athenes(), event -> {
            try {
                PlayerCite.getInstance().addPlayerToCite(CiteEnum.ATHENES, event.getWhoClicked().getName());
            } catch (PlayerAlreadyInCiteException e) {
                event.getWhoClicked().sendMessage("§cVous êtes déjà dans une cité !");
            }

            event.getWhoClicked().closeInventory();
        });

        setItem(22, new ItemBuilder(Material.SIGN)
                .setName(Formatter.divideJustFirst(ConfigurationManager.getMessage("inventory.choose.signinfo")))
                .setLore(Formatter.divideExceptFirst(ConfigurationManager.getMessage("inventory.choose.signinfo")))
                .toItemStack());

        setItem(24, alexandrie(), event -> {
            try {
                PlayerCite.getInstance().addPlayerToCite(CiteEnum.ALEXANDRIE, event.getWhoClicked().getName());
            } catch (PlayerAlreadyInCiteException e) {
                event.getWhoClicked().sendMessage("§cVous êtes déjà dans une cité !");
            }

            event.getWhoClicked().closeInventory();
        });

        setItems(getCorners(), new ItemBuilder(Material.STAINED_GLASS_PANE).setDyeColor(DyeColor.BLACK).setName(" ").toItemStack());
    }

    @Override
    public void onClose(InventoryCloseEvent event) {
        if (!PlayerCite.getInstance().hasPlayerCite(event.getPlayer().getName())) {
            Bukkit.getScheduler().runTask(Cite.getInstance(), () -> new ChooseCiteInventory().open((Player) event.getPlayer()));
        }
    }

    private ItemStack athenes() {
        if (PlayerCite.getInstance().canJoinCite(CiteEnum.ATHENES, CiteEnum.ALEXANDRIE)) {
            return new ItemBuilder(Material.TNT).setName("§f§lREJOINDRE §9§lATHENES")
                    .setLore("§1",
                            " §6§lINFORMATIONS",
                            "  §fJoueurs dans cette cité: §7" + PlayerCite.getInstance().getCiteNumber(CiteEnum.ATHENES),
                            "§2",
                            " §6§lAVANTAGES",
                            "  §7- §7Accès au Shop de Athènes. §c/shop",
                            "  §7- §7Effet de §cForce II §7en permanence.",
                            "§7...").toItemStack();
        } else {
            return new ItemBuilder(Material.TNT).setName("§f§lREJOINDRE §9§lATHENES")
                    .setLore("§c§lIL Y A TROP DE JOUEURS DANS CETTE CITE !",
                            "§1",
                            " §6§lINFORMATIONS",
                            "  §fJoueurs dans cette cité: §7" + PlayerCite.getInstance().getCiteNumber(CiteEnum.ATHENES),
                            "§2",
                            " §6§lAVANTAGES",
                            "  §7- Accès au Shop de Athènes. §c/shop",
                            "  §7- Effet de §cForce II §7en permanence.",
                            "§7...").toItemStack();
        }
    }

    private ItemStack alexandrie() {
        if (PlayerCite.getInstance().canJoinCite(CiteEnum.ALEXANDRIE, CiteEnum.ATHENES)) {
            return new ItemBuilder(Material.WORKBENCH).setName("§f§lREJOINDRE §9§lALEXANDRIE")
                    .setLore("§1",
                            " §6§lINFORMATIONS",
                            "  §fJoueurs dans cette cité: §7" + PlayerCite.getInstance().getCiteNumber(CiteEnum.ALEXANDRIE),
                            "§2",
                            " §6§lAVANTAGES",
                            "  §7- Accès au Shop de Alexandrie. §c/shop",
                            "  §7- Effet de §cSpeed II §7en permanence.",
                            "§7...").toItemStack();
        } else {
            return new ItemBuilder(Material.WORKBENCH).setName("§f§lREJOINDRE §9§lALEXANDRIE")
                    .setLore("§c§lIL Y A TROP DE JOUEURS DANS CETTE CITE !",
                            "§1",
                            " §6§lINFORMATIONS",
                            "  §fJoueurs dans cette cité: §7" + PlayerCite.getInstance().getCiteNumber(CiteEnum.ALEXANDRIE),
                            "§2",
                            " §6§lAVANTAGES",
                            "  §7- Accès au Shop de Alexandrie. §c/shop",
                            "  §7- Effet de §cSpeed II §7en permanence.",
                            "§7...").toItemStack();
        }
    }
}
