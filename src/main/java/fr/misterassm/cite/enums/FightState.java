package fr.misterassm.cite.enums;

public enum FightState {

    NOT_START
    , WAITING
    , FIGHT
    ;

}
