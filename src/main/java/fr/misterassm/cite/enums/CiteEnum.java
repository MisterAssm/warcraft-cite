package fr.misterassm.cite.enums;

import java.util.Arrays;
import java.util.List;

public enum CiteEnum {

    ATHENES("athenes", "§9Athènes", "group.athenes")
    , ALEXANDRIE("alexandrie", "§6Alexandrie", "group.alexandrie")
    ;

    private String file;
    private String name;
    private String group;

    CiteEnum(String file, String name, String group) {
        this.file = file;
        this.name = name;
        this.group = group;
    }

    public static CiteEnum getCiteByName(String name) {
        return Arrays.stream(values()).filter(cite ->
                cite.getName().equalsIgnoreCase(name) || cite.getFile().equalsIgnoreCase(name)).findAny()
                .orElse(null);
    }

    public static List<CiteEnum> getCites() {
        return Arrays.asList(CiteEnum.values());
    }

    public String getFile() { return file; }
    public String getName() { return name; }
    public String getGroup() { return group; }

}
