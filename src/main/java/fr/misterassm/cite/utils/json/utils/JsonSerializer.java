package fr.misterassm.cite.utils.json.utils;

import org.json.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class JsonSerializer {

    /**
     * @param object
     * @return
     */

    public JSONObject serialize(Object object) {
        JSONObject jsonObject = new JSONObject();
        try {
            Field[] fields = object.getClass().getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                if (List.class.isAssignableFrom(field.getType())) {
                    jsonObject.put(field.getName(), serializeList(field, object));
                } else if(isBasicClass(field.getType())) {
                    jsonObject.put(field.getName(), field.get(object));
                }else if(Map.class.isAssignableFrom(field.getType())){
                    jsonObject.put(field.getName(), serializeMap(field, object));
                } else if(field.getType().isEnum()) {
                    jsonObject.put(field.getName(), field.get(object).toString());
                }else {
                    jsonObject.put(field.getName(), serializeData(field, object));
                }
            }
            return jsonObject;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @param field
     * @param object
     * @return
     * @throws IllegalAccessException
     */
    private Map serializeMap(Field field, Object object) throws IllegalAccessException {
        Map<?,?> map = (Map<?, ?>) field.get(object);
        ParameterizedType parameterizedType = (ParameterizedType) field.getGenericType();
        if (isBasicClass((Class<?>) parameterizedType.getActualTypeArguments()[0]) && isBasicClass((Class<?>) parameterizedType.getActualTypeArguments()[1])) return map;

        Map m = new HashMap();
        for (Map.Entry<?, ?> entry : map.entrySet()) {
            m.put((isBasicClass(entry.getKey().getClass()) ? entry.getKey(): serializeData(entry.getKey())), (isBasicClass(entry.getValue().getClass()) ? entry.getValue(): serializeData(entry.getValue())));
        }
        return m;
    }

    /**
     *
     * @param field
     * @param object
     * @return
     * @throws IllegalAccessException
     */
    private List serializeList(Field field, Object object) throws IllegalAccessException {
        List list = (List) field.get(object);
        ParameterizedType parameterizedType = (ParameterizedType) field.getGenericType();
        Class<?> aClass = (Class<?>) parameterizedType.getActualTypeArguments()[0];
        if (isBasicClass(aClass)) return list;

        List arrayList = new ArrayList<>();
        for (Object o : list) {
            arrayList.add(serializeData(o));
        }
        return arrayList;
    }

    /**
     *
     * @param aClass
     * @return
     */

    private boolean isBasicClass(Class<?> aClass){
        if(aClass == String.class || aClass == int.class || aClass == float.class|| aClass == boolean.class || aClass == char.class || aClass == byte.class || aClass == short.class || aClass == long.class || aClass == double.class) return true;
        return false;
    }

    /**
     *
     * @param field
     * @param object
     * @return
     * @throws IllegalAccessException
     */
    private JSONObject serializeData(Field field, Object object) throws IllegalAccessException {
        if (field.get(object) != null) {
            return new JsonUtil().serialize(field.get(object));
        }
        return null;
    }

    /**
     *
     * @param object
     * @return
     * @throws IllegalAccessException
     */
    private JSONObject serializeData(Object object) throws IllegalAccessException {
        JSONObject jsonObject = new JSONObject();
        for (Field declaredField : object.getClass().getDeclaredFields()) {
            declaredField.setAccessible(true);
            jsonObject.put(declaredField.getName(), declaredField.get(object));
        }
        return jsonObject;
    }

}
