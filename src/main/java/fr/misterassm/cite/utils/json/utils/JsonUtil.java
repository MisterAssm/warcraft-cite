package fr.misterassm.cite.utils.json.utils;

import com.google.gson.Gson;
import org.json.JSONObject;


public class JsonUtil {

    private Gson gson;
    public JsonUtil() {
        this.gson = new Gson();
    }

    public JSONObject serialize(Object object) {
        return new JsonSerializer().serialize(object);
    }

    public <T> T deserialize(JSONObject jsonObject, Class<T> aclass) {
        return new JsonDeserializer(gson).deserialize(jsonObject, aclass);
    }


}
