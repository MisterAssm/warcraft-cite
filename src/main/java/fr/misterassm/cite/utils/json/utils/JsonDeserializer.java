package fr.misterassm.cite.utils.json.utils;

import com.google.gson.Gson;
import org.json.JSONObject;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.function.Supplier;

class JsonDeserializer {

    private Gson gson;
    private Map<Class<?>, Supplier<?>> classSupplierMap;
    public JsonDeserializer(Gson gson) {
        this.gson = gson;
        this.classSupplierMap = new HashMap<>();

        this.classSupplierMap.put(String.class, () -> "");
        this.classSupplierMap.put(int.class, () -> 0);
        this.classSupplierMap.put(float.class, () -> 0);
        this.classSupplierMap.put(boolean.class, () -> false);
        this.classSupplierMap.put(char.class, () -> ' ');
        this.classSupplierMap.put(short.class, () -> 0);
        this.classSupplierMap.put(byte.class, () -> 0);
        this.classSupplierMap.put(long.class, () -> 0L);
        this.classSupplierMap.put(double.class, () -> 0);

        this.classSupplierMap.put(Map.class, HashMap::new);
        this.classSupplierMap.put(HashMap.class, HashMap::new);
        this.classSupplierMap.put(LinkedHashMap.class, LinkedHashMap::new);
        this.classSupplierMap.put(Hashtable.class, Hashtable::new);


        this.classSupplierMap.put(List.class, ArrayList::new);
        this.classSupplierMap.put(ArrayList.class, ArrayList::new);
        this.classSupplierMap.put(LinkedList.class, LinkedList::new);
        this.classSupplierMap.put(Vector.class, Vector::new);
        this.classSupplierMap.put(Stack.class, Stack::new);

    }

    /**
     * Deserialize the JSONObject and complete the class as a parameter in class object
     * @param jsonObject
     * @param aclass
     * @return object
     */
    public <T> T deserialize(JSONObject jsonObject, Class<T> aclass) {
        try {
            Object object = newInstance(aclass);
            for (Map.Entry<String, Object> entry : jsonObject.toMap().entrySet()) {
                Field field = object.getClass().getDeclaredField(entry.getKey());
                field.setAccessible(true);
                if(isBasicClass(field.getType())){
                    field.set(object, entry.getValue());
                }else if (Map.class.isAssignableFrom(field.getType()) || List.class.isAssignableFrom(field.getType()) || field.getType().isEnum()) {
                    field.set(object, deserializeListMapEnum(field, entry.getValue()));
                }else {
                    field.set(object,deserializeData(field, entry.getValue()));
                }
            }
            return aclass.cast(object);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Deserialize maps, list, and enum to object.
     * @param field
     * @param value
     * @return Object
     */
    private Object deserializeListMapEnum(Field field, Object value) {
        return gson.fromJson(value.toString(), field.getType());
    }

    /**
     *
     * @param aClass
     * @return
     */

    private boolean isBasicClass(Class<?> aClass){
        if(aClass == String.class || aClass == int.class || aClass == float.class|| aClass == boolean.class || aClass == char.class || aClass == byte.class || aClass == short.class || aClass == long.class || aClass == double.class) return true;
        return false;
    }

    /**
     * Deserialize an object in class object
     * @param field
     * @param value
     * @return Object
     * @throws IllegalAccessException
     * @throws NoSuchFieldException
     * @throws NoSuchMethodException
     * @throws InstantiationException
     * @throws InvocationTargetException
     */

    private Object deserializeData(Field field, Object value) throws Exception {
        Object object = gson.fromJson(value.toString(), field.getType());
        Map<String, Object> objectMap = new HashMap<>();
        for (Field f : object.getClass().getDeclaredFields()) {
            f.setAccessible(true);
            if (value.toString().contains(f.getName())) {
                objectMap.put(f.getName(), f.get(object));
            }
        }
        Object o = newInstance(field.getType());
        for (Map.Entry<String, Object> entry : objectMap.entrySet()) {
            if (containsField(entry.getKey(), Arrays.asList(o.getClass().getDeclaredFields()))) {
                Field f = o.getClass().getDeclaredField(entry.getKey());
                f.setAccessible(true);
                f.set(o, entry.getValue());
            }
        }
        return o;
    }

    /**
     * Check if the key is a field
     * @param s
     * @param fields
     * @return boolean
     */

    private boolean containsField(String s, List<Field> fields){
        for (Field field : fields) {
            if (field.getName().equals(s)) return true;
        }
        return false;
    }

    /**
     *
     * @param aClass
     * @return object
     * @throws NoSuchMethodException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws InstantiationException
     */
    private Object newInstance(Class<?> aClass) throws Exception {
        Constructor constructor = aClass.getDeclaredConstructor(aClass.getConstructors()[0].getParameterTypes());
        return constructor.newInstance(generateComponent(aClass.getConstructors()[0].getParameterTypes()));
    }



    /**
     *
     * @param classes
     * @return object[]
     */
    private Object[] generateComponent(Class... classes) {
        Object[] objects = new Object[classes.length];
        for (int i = 0; i < classes.length; i++) {
            Class aClass = classes[i];
            if(this.classSupplierMap.containsKey(aClass)) objects[i] = this.classSupplierMap.get(aClass).get();
        }
        return objects;
    }

}
