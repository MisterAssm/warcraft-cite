package fr.misterassm.cite.utils;

import java.util.*;

public class Formatter {

    private static final NavigableMap<Long, String> suffixes = new TreeMap<>();
    static {
        suffixes.put(1_000L, "k");
        suffixes.put(1_000_000L, "M");
        suffixes.put(1_000_000_000L, "B");
        suffixes.put(1_000_000_000_000L, "T");
        suffixes.put(1_000_000_000_000_000L, "P");
        suffixes.put(1_000_000_000_000_000_000L, "E");
    }

    public static String numberFormat(long value) {
        //Long.MIN_VALUE == -Long.MIN_VALUE so we need an adjustment here
        if (value == Long.MIN_VALUE) return numberFormat(Long.MIN_VALUE + 1);
        if (value < 0) return "-" + numberFormat(-value);
        if (value < 1000) return Long.toString(value); //deal with easy case

        Map.Entry<Long, String> e = suffixes.floorEntry(value);
        Long divideBy = e.getKey();
        String suffix = e.getValue();

        long truncated = value / (divideBy / 10); //the number part of the output times 10
        boolean hasDecimal = truncated < 100 && (truncated / 10d) != (truncated / 10);
        return hasDecimal ? (truncated / 10d) + suffix : (truncated / 10) + suffix;
    }

    public static String divideJustFirst(String value) {
        List<String> list = new ArrayList<>();
        if (!value.contains("\\n"))
            return value;
        byte b;
        int j;
        String[] arrayOfString;
        for (j = (arrayOfString = value.split("\\\\n")).length, b = 0; b < j; ) {
            String s = arrayOfString[b];
            list.add(s);
            b++;
        }
        return list.get(0);
    }

    public static List<String> divideExceptFirst(String value) {
        List<String> list = new ArrayList<>();
        if (!value.contains("\\n"))
            return list;
        int i = 0;
        byte b;
        int j;
        String[] arrayOfString;
        for (j = (arrayOfString = value.split("\\\\n")).length, b = 0; b < j; ) {
            String s = arrayOfString[b];
            if (i != 0)
                list.add(s);
            i++;
            b++;
        }
        return list;
    }

}
