package fr.misterassm.cite;

import fr.misterassm.cite.enums.FightState;
import fr.misterassm.cite.managers.ConfigurationManager;
import fr.misterassm.cite.managers.RegisterManager;
import fr.misterassm.cite.managers.cite.CiteManager;
import fr.misterassm.cite.managers.fight.FightManager;
import fr.misterassm.cite.managers.player.PlayerManager;
import fr.misterassm.cite.utils.inventory.FastInvManager;
import fr.misterassm.cite.utils.scoreboard.FastBoard;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("ResultOfMethodCallIgnored")
public class Cite extends JavaPlugin {

    private static Cite instance;

    private final Map<String, FastBoard> boards = new HashMap<>();

    private FightState fightState;
    private PlayerManager playerManager;
    private CiteManager citeManager;
    private FightManager fightManager;

    @Override
    public void onEnable() {
        instance = this;

        if (!getDataFolder().exists())
            getDataFolder().mkdir();

        this.fightState = FightState.NOT_START;

        this.playerManager = new PlayerManager();
        this.citeManager = new CiteManager();
        this.fightManager = new FightManager();

        this.citeManager.loadFile();
        this.playerManager.loadFile();

        FastInvManager.register(this);

        new ConfigurationManager().createConfigurationFile();
        new RegisterManager().register();
    }

    @Override
    public void onDisable() {
        this.playerManager.saveFile();
    }

    public static Cite getInstance() { return instance; }

    public FightState getFightState() { return fightState; }
    public void setFightState(FightState fightState) { this.fightState = fightState; }

    public Map<String, FastBoard> getBoards() { return boards; }

}
